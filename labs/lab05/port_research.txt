Following is the Active Internet connections (servers and established) copied from log.txt
--------------------------------------------------------------------------------------------------------------------------------------------------
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.1.1:53            0.0.0.0:*               LISTEN      1014/dnsmasq (*PORT 1*)   
tcp        0      0 0.0.0.0:21              0.0.0.0:*               LISTEN      915/pure-ftpd (SERV  (*PORT2*)
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      866/sshd  (*PORT3*)     
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      483/cupsd (*PORT4*)      
tcp        0      0 10.0.2.15:37362         10.0.2.2:4444           TIME_WAIT   -   (*PORT5*)            
tcp        0      0 10.0.2.15:37361         10.0.2.2:4444           TIME_WAIT   -               
tcp        0      0 10.0.2.15:37360         10.0.2.2:4444           TIME_WAIT   -               
tcp        0      0 10.0.2.15:37363         10.0.2.2:4444           ESTABLISHED 2833/nc         
tcp6       0      0 :::21                   :::*                    LISTEN      915/pure-ftpd (SERV
tcp6       0      0 :::22                   :::*                    LISTEN      866/sshd        
udp        0      0 0.0.0.0:40832           0.0.0.0:*                           743/dhclient    
udp        0      0 127.0.1.1:53            0.0.0.0:*                           1014/dnsmasq    
udp        0      0 0.0.0.0:68              0.0.0.0:*                           743/dhclient    
udp        0      0 0.0.0.0:68              0.0.0.0:*                           744/dhclient    
udp        0      0 0.0.0.0:7293            0.0.0.0:*                           744/dhclient    
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           472/avahi-daemon: r
udp        0      0 0.0.0.0:53767           0.0.0.0:*                           472/avahi-daemon: r
udp        0      0 0.0.0.0:631             0.0.0.0:*                           518/cups-browsed
udp6       0      0 :::7293                 :::*                                743/dhclient    
udp6       0      0 :::5353                 :::*                                472/avahi-daemon: r
udp6       0      0 :::51450                :::*                                472/avahi-daemon: r
udp6       0      0 :::8883                 :::*                                744/dhclient  
-------------------------------------------------------------------------------------------------------------------------------------------------
PORT 1(Also labeled above in the log)
tcp        0      0 127.0.1.1:53            0.0.0.0:*               LISTEN      1014/dnsmasq
Essentilly this is Port 53 and was listening through tcp protocol. This was started by process 1014 called dnsmasq. 
Basically, used for DNS service.

PORT 2(Also labeled above in the log)
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      866/sshd 
Port 21 is opened through tcp protocol which was called by the PID 915 named pure-ftpd. This is basically ftp server used for linux.

PORT 3(Labeled above in the log)
This is port 22. Its a well known port used for SSH. It basically listening on 0.0.0.0. All in all, this takes care of remote into the systems.

PORT 4(Labeled above in the log)
0 127.0.0.1:631, This is essentially port 631 listening through tcp protocol. 483/cupsd, its opened by PID 483 called cupsd. Essentially, cupsd 
is "A daemon for browsing the Bokour brodcasts of shared printers" (opensource.com)

PORT 5(Labeled above in the log)
10.0.2.2:4444, Port 4444 is opened basically the netcat listener on our machine (2833/nc).



