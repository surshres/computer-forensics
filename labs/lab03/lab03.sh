# Group : Suraaj Shrestha and Deodatta Baral
# Computer Forensics Lab 3

#!/usr/bin/env bash
PARAMA=$1
PARAMB=$2

#Determine which argument is larger numerically, and print that argument to the console

if [ $PARAMA -ge $PARAMB ]
then
      echo The larger number is: $PARAMA
else
      echo The larger number is: $PARAMB
fi

#Take the first argument to the Bash script and write a for loop that iterates as many times as specified by that variable. Each iteration of the loop should print "Bash is great" to the console

for((i = 1; i <= $PARAMA; i++));
do
    echo Bash is great
done

#The second argument will be passed as a parameter to a newly created function named square. The results of the square function will be printed to the console via a global variable set inside the square function

square () {
  local squaredNum=$(( $1*$1 ))
  echo $squaredNum
}

squareVar=$(square "$PARAMB")
echo "The square value: $squareVar"

#Create an array of the first two script arguments and assign it to a variable. Print out the length of element 1 in the array, as well as how many elements are in the array

eleArr=([0]=$PARAMA [1]=$PARAMB)
echo "Length of element 1 in the array: ${#eleArr[1]}"
echo "Elements in the array: ${#eleArr[@]}"
